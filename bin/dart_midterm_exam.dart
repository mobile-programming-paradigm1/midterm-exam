import 'package:dart_midterm_exam/dart_midterm_exam.dart' as dart_midterm_exam;

import 'dart:io';

List tokenizing(String wordtokens) {
  var tokenisnum1 = wordtokens.split('');

  var tokenisnum2 = new List<String>.filled(0, "A", growable: true);
  for (var token in tokenisnum1) {
    if (token != " ") {
      tokenisnum2.add(token);
    }
  }

  return tokenisnum2;
}

bool isInteger(String str) {
  for (var i = 0; i <= 9; i++) {
    try {
      if (int.parse(str) == i) {
        return true;
      }
    } on Exception catch (e) {}
  }
  return false;
}

bool isOperator(String str) {
  for (String t in {"+", "-", "*", "/", "%", "^"}) {
    if (str.compareTo(t) == 0) {
      return true;
    }
  }
  return false;
}

int priority(String str) {
  switch (str) {
    case "+":
    case "-":
      return 1;
    case "*":
    case "/":
    case "%":
      return 2;
    case "^":
      return 3;
    default:
      return 0;
  }
}

List infixToPostfix(List infix) {
  var operatorList = new List<String>.filled(0, "A", growable: true);
  var postfix = new List<String>.filled(0, "A", growable: true);

  for (var token in infix) {
    if (isInteger(token)) {
      postfix.add(token);
    }
    if (isOperator(token)) {
      while (operatorList.isNotEmpty &&
          (operatorList.last.compareTo("(") != 0) &&
          (priority(token) <= priority(operatorList.last))) {
        postfix.add(operatorList.removeLast());
      }
      operatorList.add(token);
    }
    if (token.compareTo("(") == 0) {
      operatorList.add(token);
    }
    if (token.compareTo(")") == 0) {
      while (operatorList.last.compareTo("(") != 0) {
        postfix.add(operatorList.removeLast());
      }
      operatorList.removeLast();
    }
  }
  while (operatorList.isNotEmpty) {
    postfix.add(operatorList.removeLast());
  }

  return postfix;
}

int addOperator(int left, int right, String token) {
  switch (token) {
    case "+":
      return left + right;
    case "-":
      return left - right;
    case "*":
      return left * right;
    case "/":
      return left ~/ right;
    case "%":
      return left % right;
    case "^":
      for (var i = 1; i <= right; i++) {
        left *= left;
      }
      return left;
    default:
      return 0;
  }
}

int evaluatePostfix(List postfix) {
  var totalToken = new List<int>.filled(0, 0, growable: true);

  for (var token in postfix) {
    if (isInteger(token)) {
      var intToken = int.parse(token);
      totalToken.add(intToken);
    } else {
      var right = totalToken.removeLast();
      var left = totalToken.removeLast();
      var result = addOperator(left, right, token);
      totalToken.add(result);
    }
  }
  return totalToken.first;
}

void main(List<String> arguments) {
  String wordtokens = stdin.readLineSync()!;
  print("\n input a Token : " + (wordtokens).toString());
  var tokens = tokenizing(wordtokens);
  print("Tokenizing a String : $tokens");
  var postfix = infixToPostfix(tokens);
  print(" Infix to Postfix :  $postfix");
  var totalToken = evaluatePostfix(postfix);
  print("Evaluate of postfix  : $totalToken");
}
